package com.example.fundrecords;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private ArrayList<Fund> mFundsList;
    private ArrayList<Fund> mFilteredFundsList;
    private boolean mFilter = false;

    private SearchView searchView;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextView = (TextView) findViewById(R.id.textView);

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {
                //initTestData();
                initData();
                return null;
            };

            @Override
            protected void onPostExecute(Void v) {
                super.onPostExecute(v);
                initViews();
            }
        }.execute();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo( getComponentName() ));
        searchView.setIconifiedByDefault(true);
        searchView.setQueryRefinementEnabled(true);
        searchView.setSubmitButtonEnabled(true);

        searchView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener()
        {
            @Override
            public void onViewAttachedToWindow(View arg0) {
                // search opened
            }

            @Override
            public void onViewDetachedFromWindow(View arg0) {
                // search closed
                if (mFilter) {
                    mFilter = false;
                    updateAdapter(mFundsList);
                }
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort:
                showSortDialog();
                break;
        }

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String query = intent.getStringExtra(SearchManager.QUERY);
        search(query);
    }

    private void initTestData() {
        try {
            DateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

            mFundsList = new ArrayList<>();
            mFundsList.add(new Fund("Alice", 12.15, formatter.parse("29-APR-1994")));
            mFundsList.add(new Fund("Bob", 10.15, formatter.parse("9-APR-1994")));
            mFundsList.add(new Fund("Clara", 15.15, formatter.parse("19-APR-1994")));
            mFundsList.add(new Fund("Drishti", 19.15, formatter.parse("9-APR-1994")));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void initData() {
        AssetManager assetManager = getResources().getAssets();
        try {
            DateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

            mFundsList = new ArrayList<>();

            StringBuilder buf = new StringBuilder();
            InputStream json = assetManager.open("data.txt");
            BufferedReader in = new BufferedReader(new InputStreamReader(json, "UTF-8"));
            String line;

            while ((line = in.readLine()) != null) {
                try {
                    String[] arr = line.split(";");
                    if (arr.length >= 7) {
                        String name = arr[3].trim();

                        String value = arr[4].trim();
                        double d = Double.valueOf(value);
                        DecimalFormat newFormat = new DecimalFormat("#.####");
                        d =  Double.valueOf(newFormat.format(d));

                        String date = arr[7].trim();

                        Fund fund = new Fund(name, d, formatter.parse(date));
                        mFundsList.add(fund);
                    }
                } catch (Exception e) {
                    Log.e("debuglogging"," : Error while reading line" + e.toString());
                }

            }

            in.close();

        } catch (Exception e) {
            Log.e("debuglogging"," : Error while getting data" + e.toString());
        }
    }

    private void initViews() {
        mTextView.setVisibility(View.GONE);
        mRecyclerView = (RecyclerView) findViewById(R.id.homeRecyclerView);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        updateAdapter(mFundsList);
    }

    private void updateAdapter(ArrayList list) {
        mAdapter = new FundsAdapter(list);
        mRecyclerView.setAdapter(mAdapter);
        if (!list.isEmpty()) {
            mTextView.setVisibility(View.GONE);
        } else {
            mTextView.setText("No result found");
            mTextView.setVisibility(View.VISIBLE);
        }
    }

    private void showSortDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose option");

        String[] sortOptions = {"Sort by value", "Sort by date", "Reverse"};
        builder.setItems(sortOptions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        // Sort by value
                        sortByValue();
                        break;
                    case 1:
                        // Sort by date
                        sortByDate();
                        break;
                    case 2:
                        reverse();
                        break;
                }
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void search(String query) {
        mFilteredFundsList = new ArrayList<>();
        for (Fund fund : mFundsList) {
            if ((fund.getName().toLowerCase()).contains(query.toLowerCase())) {
                mFilteredFundsList.add(fund);
            }
        }
        mFilter = true;
        updateAdapter(mFilteredFundsList);
    }

    private void sortByValue() {
        new AsyncTask<Void, Void, ArrayList<Fund>>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected ArrayList<Fund> doInBackground(Void... params) {
                try {
                    if (!mFilter) {
                        mFilteredFundsList = new ArrayList<>(mFundsList);
                    }
                    Collections.sort(mFilteredFundsList, new Comparator<Fund>() {
                        public int compare(Fund v1, Fund v2) {
                            int returnValue = 0;

                            if (v1.getValue() < v2.getValue()) {
                                returnValue =  -1;
                            } else if (v1.getValue() > v2.getValue()) {
                                returnValue =  1;
                            } else if (v1.getValue() == v2.getValue()) {
                                returnValue =  0;
                            }

                            return returnValue;
                        }
                    });

                    return mFilteredFundsList;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            };

            @Override
            protected void onPostExecute(ArrayList<Fund> fundsList) {
                super.onPostExecute(fundsList);
                mFilter = true;
                updateAdapter(mFilteredFundsList);
            }
        }.execute();
    }

    private void sortByDate() {
        new AsyncTask<Void, Void, ArrayList<Fund>>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected ArrayList<Fund> doInBackground(Void... params) {
                try {
                    if (!mFilter) {
                        mFilteredFundsList = new ArrayList<>(mFundsList);
                    }
                    Collections.sort(mFilteredFundsList, new Comparator<Fund>() {
                        public int compare(Fund v1, Fund v2) {
                            return v1.getDate().compareTo(v2.getDate());
                        }
                    });
                    return mFilteredFundsList;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            };

            @Override
            protected void onPostExecute(ArrayList<Fund> fundsList) {
                super.onPostExecute(fundsList);
                mFilter = true;
                updateAdapter(mFilteredFundsList);
            }
        }.execute();
    }

    private void reverse() {
        if (!mFilter) {
            mFilteredFundsList = new ArrayList<>(mFundsList);
        }

        Collections.reverse(mFilteredFundsList);
        mFilter = true;
        updateAdapter(mFilteredFundsList);
    }

    @Override
    public void onBackPressed() {
        if (mFilter) {
            updateAdapter(mFundsList);
            mFilter = false;
        } else {
            super.onBackPressed();
        }
    }
}
