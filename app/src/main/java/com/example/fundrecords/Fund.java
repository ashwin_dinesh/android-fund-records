package com.example.fundrecords;

import android.util.Log;

import java.time.LocalDate;
import java.util.Date;

/**
 * Created by ashwin on 13/11/17.
 */

public class Fund {

    private String name, dateString;
    private double value;
    private Date date;

    public Fund() { }

    public Fund(String name, double value, Date date) {
        this.name = name;
        this.value = value;
        this.date = date;
        this.dateString = toDateString(date);
    }

    private String toDateString(Date date) {
        String d = String.valueOf(date);
        //d = d.replaceAll(" ", "");
        //Log.d("debuglogging", "date: " + d);
        String[] dateArray = d.split(" ");
        d = dateArray[2] + "-" + dateArray[1].toUpperCase() + "-" + dateArray[5];
        return d;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
        setDateString(toDateString(date));
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    @Override
    public String toString() {
        return name + " : " + value + " : " + dateString;
    }
}
