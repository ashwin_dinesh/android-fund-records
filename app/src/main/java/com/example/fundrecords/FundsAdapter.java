package com.example.fundrecords;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by ashwin on 13/11/17.
 */

public class FundsAdapter extends RecyclerView.Adapter<FundsAdapter.FundViewHolder> {

    private ArrayList<Fund> mFundsList;

    public FundsAdapter(ArrayList<Fund> fundsList) {
        mFundsList = fundsList;
    }

    public class FundViewHolder extends RecyclerView.ViewHolder {
        public TextView mNameTextView, mValueTextView, mDateTextView;

        public FundViewHolder(View view) {
            super(view);
            mNameTextView = (TextView) view.findViewById(R.id.nameTextView);
            mValueTextView = (TextView) view.findViewById(R.id.valueTextView);
            mDateTextView = (TextView) view.findViewById(R.id.dateTextView);
        }
    }

    @Override
    public FundViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fund_row, parent, false);

        return new FundViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FundViewHolder holder, int position) {
        Fund fund = mFundsList.get(position);
        (holder.mNameTextView).setText(fund.getName());
        (holder.mValueTextView).setText(String.valueOf(fund.getValue()));
        (holder.mDateTextView).setText(fund.getDateString());
    }

    @Override
    public int getItemCount() {
        return mFundsList.size();
    }

}
